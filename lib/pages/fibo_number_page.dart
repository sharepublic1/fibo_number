import 'package:fibonacci_number/pages/widget/fibo_widget.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../providers/providers.dart';

class FiboNumberPage extends StatefulWidget {
  const FiboNumberPage({super.key});

  @override
  State<FiboNumberPage> createState() => _FiboNumberPageState();
}

class _FiboNumberPageState extends State<FiboNumberPage> {
  late final ScrollController scrollController;

  @override
  void initState() {
    super.initState();
    scrollController = ScrollController();
    context.read<FiboProvider>().genFiboNumbers();
  }

  @override
  void dispose() {
    scrollController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Scaffold(
      appBar: AppBar(
        title: const Text('Example'),
        centerTitle: true,
      ),
      body: Consumer<FiboProvider>(
        builder: (_, fiboProvider, __) {
          return ListView.builder(
              controller: scrollController,
              shrinkWrap: true,
              itemCount: fiboProvider.fiboNumbers.length,
              itemBuilder: (context, index) {
                return buildFiboNumber(
                    index: index,
                    context: context,
                    fiboMd: fiboProvider.fiboNumbers[index],
                    provider: fiboProvider,
                    scrollController: scrollController
                );
              });
        },
      ),
    ));
  }
}
