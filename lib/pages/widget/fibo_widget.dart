import 'package:fibonacci_number/models/fibo_model.dart';

import 'package:flutter/material.dart';

import '../../providers/providers.dart';

Widget buildFiboNumber({required int index,
  required BuildContext context,
  required FiboModel fiboMd,
  required FiboProvider provider,
  required ScrollController scrollController
}) {
  return SizedBox(
    height: 70,
    child: ListTile(
      onTap: () {
        provider.addFiboNumber(index, fiboMd);
        List<FiboModel> fiboNumbers = provider.getFiboNumberByType(fiboMd.type);
        showModalBottomSheet(
            context: (context),
            isScrollControlled: true,
            builder: (_) {
              return DraggableScrollableSheet(
                expand: false,
                builder: (context, scrollControllerModal) {
                  provider.gotoWidget(
                    scrollController: scrollControllerModal,
                    fiboList: fiboNumbers,
                    fiboMd: fiboMd,
                    boxHeight: 70,
                  );

                  return ListView.builder(
                      controller: scrollControllerModal,
                      itemCount: fiboNumbers.length,
                      shrinkWrap: true,

                      itemBuilder: (context, index) {
                        return buildFiboNumberBottomSheet(
                            index: index,
                            context: context,
                            fiboMd: fiboNumbers[index],
                            provider: provider,
                            scrollController: scrollController);
                      });
                },
              );
            });
      },
      title: Text('index: ${fiboMd.indexFibo},Number: ${fiboMd.number}'),
      trailing: Icon(_getIcon(fiboMd.type)),
      tileColor: provider.indexFiboSelected == fiboMd.indexFibo
          ? Colors.red
          : Colors.white,
    ),
  );
}

Widget buildFiboNumberBottomSheet({required int index,
  required BuildContext context,
  required FiboModel fiboMd,
  required FiboProvider provider,
  required ScrollController scrollController
  }) {
  return SizedBox(
    height: 70,
    child: ListTile(
      onTap: () {
        provider.removeFiboNumber(index, fiboMd);
        Navigator.pop(context);
        provider.gotoWidget(
            scrollController: scrollController,
            fiboList: provider.fiboNumbers,
            fiboMd: fiboMd,
            boxHeight: 70);
      },
      title: Text('Number: ${fiboMd.number}'),
      subtitle: Text('index: ${fiboMd.indexFibo}'),
      trailing: Icon(_getIcon(fiboMd.type)),
      tileColor: provider.indexFiboSelected == fiboMd.indexFibo
          ? Colors.green
          : Colors.white,
    ),
  );
}

IconData _getIcon(FiBoType type) {
  switch (type) {
    case FiBoType.square:
      return Icons.square_outlined;
    case FiBoType.cross:
      return Icons.close;
    case FiBoType.circle:
      return Icons.circle;
  }
}

