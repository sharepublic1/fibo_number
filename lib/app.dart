import 'package:fibonacci_number/pages/fibo_number_page.dart';
import 'package:fibonacci_number/providers/providers.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';


class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      create: (_) => FiboProvider(),
      child: MaterialApp(
        theme: ThemeData(
          useMaterial3: true,
        ),
        home: const FiboNumberPage(),
      ),
    );
  }
}
