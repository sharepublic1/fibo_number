import 'package:equatable/equatable.dart';

enum FiBoType { square, cross, circle }

final class FiboModel extends Equatable {
  final int indexFibo;
  final int number;
  final FiBoType type;

  const FiboModel(
      {required this.indexFibo, required this.number, required this.type});

  @override
  List<Object?> get props => [indexFibo, number, type];
}
