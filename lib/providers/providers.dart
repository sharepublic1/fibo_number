import 'package:fibonacci_number/models/fibo_model.dart';
import 'package:flutter/material.dart';

class FiboProvider extends ChangeNotifier {
  List<FiboModel> fiboNumbers = [];
  List<FiboModel> fiboSquareType = [];
  List<FiboModel> fiboCrossType = [];
  List<FiboModel> fiboCycleType = [];

  int indexFiboSelected = -1;

  genFiboNumbers() {
    int number = 0;

    for (int i = 0; i < 41; i++) {
      if (i == 0) {
        number = 0;
      } else if (i == 1) {
        number = 1;
      } else {
        number = fiboNumbers[i - 1].number + fiboNumbers[i - 2].number;
      }
      FiBoType type = _getFiboType(i);
      FiboModel fiboNumber =
          FiboModel(indexFibo: i, number: number, type: type);
      fiboNumbers.add(fiboNumber);
    }
  }

  addFiboNumber(int index, FiboModel fiboMd) {
    switch (fiboMd.type) {
      case FiBoType.square:
        fiboSquareType.add(fiboMd);
        break;
      case FiBoType.cross:
        fiboCrossType.add(fiboMd);
        break;
      case FiBoType.circle:
        fiboCycleType.add(fiboMd);
        break;
    }
    fiboNumbers.removeAt(index);
    indexFiboSelected = fiboMd.indexFibo;
    notifyListeners();
  }

  removeFiboNumber(int index, FiboModel fiboMd) {
    switch (fiboMd.type) {
      case FiBoType.square:
        fiboSquareType.removeAt(index);
        break;
      case FiBoType.cross:
        fiboCrossType.removeAt(index);
        break;
      case FiBoType.circle:
        fiboCycleType.removeAt(index);
        break;
    }
    indexFiboSelected = fiboMd.indexFibo;
    fiboNumbers.add(fiboMd);
    fiboNumbers.sort((a, b) => a.indexFibo.compareTo(b.indexFibo));
    notifyListeners();
  }

  gotoWidget(
      {required ScrollController scrollController,
      required List<FiboModel> fiboList,
      required FiboModel fiboMd,
      required double boxHeight}) {
    int index = fiboList.indexOf(fiboMd);

    WidgetsBinding.instance.addPostFrameCallback((_) {
      if (scrollController.hasClients) {
        scrollController.animateTo(index * boxHeight,
            duration: const Duration(seconds: 1), curve: Curves.ease);
      }
    });
  }

  List<FiboModel> getFiboNumberByType(FiBoType type) {
    List<FiboModel> fiboList = [];
    switch (type) {
      case FiBoType.square:
        fiboList = fiboSquareType;
      case FiBoType.cross:
        fiboList = fiboCrossType;
      case FiBoType.circle:
        fiboList = fiboCycleType;
    }

    fiboList.sort((a, b) => a.indexFibo.compareTo(b.indexFibo));
    return fiboList;
  }

  FiBoType _getFiboType(int count) {
    switch (count) {
      case 0:
        return FiBoType.circle;
      case 1:
        return FiBoType.square;
      case 2:
        return FiBoType.square;
      case 3:
        return FiBoType.cross;
      case 4:
        return FiBoType.circle;
      case 5:
        return FiBoType.cross;
      case 6:
        return FiBoType.cross;
      case 7:
        return FiBoType.square;
      case 8:
        return FiBoType.circle;
      case 9:
        return FiBoType.square;
      case 10:
        return FiBoType.square;
      case 11:
        return FiBoType.cross;
      case 12:
        return FiBoType.circle;
      case 13:
        return FiBoType.cross;
      case 14:
        return FiBoType.cross;
      case 15:
        return FiBoType.square;
      case 16:
        return FiBoType.circle;
      case 17:
        return FiBoType.square;
      case 18:
        return FiBoType.square;
      case 19:
        return FiBoType.cross;
      case 20:
        return FiBoType.circle;
      case 21:
        return FiBoType.cross;
      case 22:
        return FiBoType.cross;
      case 23:
        return FiBoType.square;
      case 24:
        return FiBoType.circle;
      case 25:
        return FiBoType.square;
      case 26:
        return FiBoType.square;
      case 27:
        return FiBoType.cross;
      case 28:
        return FiBoType.circle;
      case 29:
        return FiBoType.cross;
      case 30:
        return FiBoType.cross;
      case 31:
        return FiBoType.square;
      case 32:
        return FiBoType.circle;
      case 33:
        return FiBoType.square;
      case 34:
        return FiBoType.square;
      case 35:
        return FiBoType.cross;
      case 36:
        return FiBoType.circle;
      case 37:
        return FiBoType.cross;
      case 38:
        return FiBoType.cross;
      case 39:
        return FiBoType.square;
      case 40:
        return FiBoType.circle;
      default:
        return FiBoType.circle;
    }
  }
}
